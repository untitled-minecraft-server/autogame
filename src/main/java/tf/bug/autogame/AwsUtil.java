package tf.bug.autogame;

import software.amazon.awssdk.services.ec2.Ec2Client;
import software.amazon.awssdk.services.ec2.model.*;

import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AwsUtil {

    public static final Filter MINECRAFT_BASE = Filter.builder().name("tag:Base").values("minecraft").build();

    public static Set<String> instanceTypes(Ec2Client client) {
        DescribeLaunchTemplatesRequest launchTemplatesReq = DescribeLaunchTemplatesRequest.builder()
                .filters(MINECRAFT_BASE)
                .build();

        Stream<LaunchTemplate> templates = client.describeLaunchTemplatesPaginator(launchTemplatesReq)
                .stream().flatMap(r -> r.launchTemplates().stream());

        return templates
                .flatMap(template -> template.tags()
                        .stream().filter(tag -> "Type".equals(tag.key())))
                .map(Tag::value)
                .collect(Collectors.toSet());
    }

    public static Set<Instance> instancesOf(Ec2Client client, String type) {
        Filter typeFilter = Filter.builder().name("tag:Type").values(type).build();

        DescribeInstancesRequest instancesReq = DescribeInstancesRequest.builder()
                .filters(MINECRAFT_BASE, typeFilter)
                .build();

        return client.describeInstancesPaginator(instancesReq).stream()
                .flatMap(r -> r.reservations().stream())
                .flatMap(r -> r.instances().stream())
                .collect(Collectors.toSet());
    }

}
