package tf.bug.autogame;

import net.ME1312.SubServers.Bungee.SubAPI;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.plugin.Plugin;
import software.amazon.awssdk.auth.credentials.AwsCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.ec2.Ec2Client;
import software.amazon.awssdk.services.ec2.model.DescribeInstancesRequest;
import software.amazon.awssdk.services.ec2.model.Filter;

import java.util.Map;

public final class Autogame extends Plugin {

    public Ec2Client client;

    @Override
    public void onEnable() {
        SubAPI.getInstance().addListener(this::subEnable, this::subReload, this::subDisable);
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }

    public void subEnable() {
        this.client = Ec2Client.create();
        AwsUtil.instanceTypes(client).forEach(type -> {
            int num = AwsUtil.instancesOf(client, type).size();
            System.out.println("Type: " + type + "\tAvailable: " + num);
        });
    }

    public void subReload() {

    }

    public void subDisable() {
        this.client.close();
    }

}
